This file is for you to describe the thorshammer application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``thorshammer`` using easy_install::

    easy_install thorshammer

Make a config file as follows::

    paster make-config thorshammer config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
