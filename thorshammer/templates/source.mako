<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html><head>
		${h.tags.javascript_link('/scripts/jquery-1.4.3.js')}
		${h.tags.javascript_link('/scripts/shCore.js')} 
		${h.tags.javascript_link('/scripts/shAutoloader.js')}

  <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
  <title>source</title>


${h.tags.stylesheet_link('/styles/shCore.css')}
${h.tags.stylesheet_link('/styles/shThemeDefault.css')}


</head><body >
<pre class="brush: ${c.lang}; toolbar: false;">	
	${c.content}
	</pre>
<script type="text/javascript">

function path()
{
  var args = arguments,
      result = []
      ;
       
  for(var i = 0; i < args.length; i++)
      result.push(args[i].replace('@', '/scripts/'));
       
  return result
};
		SyntaxHighlighter.autoloader.apply(null, path(
  'applescript            @shBrushAppleScript.js',
  'actionscript3 as3      @shBrushAS3.js',
  'bash shell             @shBrushBash.js',
  'coldfusion cf          @shBrushColdFusion.js',
  'cpp c                  @shBrushCpp.js',
  'c# c-sharp csharp      @shBrushCSharp.js',
  'css                    @shBrushCss.js',
  'delphi pascal          @shBrushDelphi.js',
  'diff patch pas         @shBrushDiff.js',
  'erl erlang             @shBrushErlang.js',
  'groovy                 @shBrushGroovy.js',
  'java                   @shBrushJava.js',
  'jfx javafx             @shBrushJavaFX.js',
  'js jscript javascript  @shBrushJScript.js',
  'perl pl                @shBrushPerl.js',
  'php                    @shBrushPhp.js',
  'text plain             @shBrushPlain.js',
  'py python              @shBrushPython.js',
  'ruby rails ror rb      @shBrushRuby.js',
  'sass scss              @shBrushSass.js',
  'scala                  @shBrushScala.js',
  'sql                    @shBrushSql.js',
  'vb vbnet               @shBrushVb.js',
  'xml xhtml xslt html    @shBrushXml.js'
));
     SyntaxHighlighter.all();
</script>
<!--</body></html>-->
