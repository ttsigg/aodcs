<%inherit file="base.mako"/>

<div id="main">
	${h.tags.form("/project/create", method="post")}
  <h2>New Project<br>
  </h2>
  <br>
  <table>
    <tbody>
      <tr>
        <td>Name:</td>
				<td colspan="3">${h.tags.text("name")}</td>
      </tr>
      <tr>
        <td>Description<br>
        </td>
				<td colspan="3">${h.tags.textarea("desc", cols=51, rows=8)}</textarea></td>
      </tr>
      <tr>
        <td>Class:</td>
        <td>
        ${h.tags.select('course','',c.courses)}
        </td>
        <td> Group Members: </td>
        <td>
        ${h.tags.select('users',c.userid,c.users,multiple=True)}
        </td>
      </tr>
    </tbody>
  </table><br>
${h.tags.submit("Save","Save")}
${h.tags.end_form()}
</div>

</body></html>
