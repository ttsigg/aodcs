<%def name="rowodd(title,link)">
  <li class="listodd"><input type="checkbox"><a href="${link}">${title}</a></li>
</%def>

<%def name="roweven(title,link)">
  <li class="listodd"><input type="checkbox"><a href="${link}">${title}</a></li>
</%def>

<%def name="sidebar(title,items,end)">
<div id="sidebar"><strong>Navigation</strong>
<li> ${h.tags.link_to(title[1],title[0])} </li>

${list(items)}
%if type(end) is string:
<%

endtitle = 'new' 
finallink = end
%>
%else:
<%
endtitle = end[1]
finallink = end[0]
%>
%endif
${h.tags.link_to(endtitle,finallink)}
</div>
</%def>

<%def name="list(items)">

%if items is not None:
<ul>
%for item in items:
%if item is not None:
%if type(item[1]) is unicode:
<li> ${h.tags.link_to(item[1],item[0])}</li>
%else:
<li>${item[0]}</li>
	${list(item[1])}

%endif
%endif
%endfor
</ul>
%endif
</%def>
