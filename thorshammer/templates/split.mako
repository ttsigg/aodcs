<%inherit file="/base.mako"/>
<%namespace file="functions.mako" import="rowodd,roweven,sidebar"/>
${h.tags.javascript_link('/scripts/jquery-1.4.3.js')}
${h.tags.javascript_link('/scripts/shCore.js')}
${h.tags.javascript_link('/scripts/shAutoloader.js')}

${h.tags.stylesheet_link('/styles/shCore.css')}
${h.tags.stylesheet_link('/styles/shThemeDefault.css')}

${sidebar(['/view/file/'+str(c.fileid),'Files'],c.listitems,['/project/view/'+str(c.projectid),'Back'])}



<div id="src" class="syntaxhighlighter"> <%include file='source.mako'/></div><br>
<div id=docs><iframe class="docs" src="/view/doc/${c.fileid}"></iframe></div>

