<%inherit file="base.mako"/>

<div id="main">
	${h.tags.form("/faculty/create", method="post")}
  <h2>New Course<br>
  </h2>
  <br>
  <table>
    <tbody>
      <tr>
        <td>Name:</td>
				<td colspan="3">${h.tags.text("name")}</td>
      </tr>
      <tr>
        <td>Description:<br>
        </td>
				<td colspan="3">${h.tags.textarea("desc", cols=51, rows=8)}</textarea></td>
      </tr>
      <tr>
        <td>Number:</td>
        <td>
        ${h.tags.text('number')}
        </td>
        <td> Students: </td>
        <td>
        ${h.tags.select('users',c.userid,c.users,multiple=True)}
        </td>
      </tr>
    </tbody>
  </table><br>
${h.tags.submit("Save","Save")}
${h.tags.end_form()}
</div>

</body></html>
