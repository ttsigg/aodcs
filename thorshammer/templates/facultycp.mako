<%inherit file="/base.mako"/>
<%namespace file="functions.mako" import="rowodd,roweven,sidebar"/>

${sidebar(['/faculty/view/default','Courses'],c.listitems,['/faculty/newcourse','New Course'])}

<div id="main">
	<h3>${c.project}</h3>
%if c.project != 'No Project':
%for s in c.students:
${s.name}, 
%endfor
<div id="controls"><button>Delete</button><button onclick=window.location.href="/faculty/grade/${c.projectid}">grade</button>
%if c.approved == 0:
	<button onclick=window.location.href="/faculty/approve/${c.projectid}">Approve Project</button>
%elif c.approved == 1:
	<button onclick=window.location.href="/faculty/approve/${c.projectid}">Revoke Project</button>
%endif
</div>
${h.tags.form('files')}
%if c.files:
<ul>
<%
x=False
%>
%for file in c.files:
	%if x: 
			${rowodd(file.filename,'http://www.google.com')}
			<%
      x=False
   		%>
	 %else:
				${roweven(file.filename,url(controller='view',id='proj'))}
			<%
      x=True		
			%>
		%endif
%endfor
  </ul>
	%endif
</form>
%endif
</div>

</body></html>
