<%namespace file="functions.mako" import="rowodd,roweven,sidebar"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html><head>
		<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">
		<link rel="stylesheet" href="/styles/style.css" type="text/css">
		<title>Portfolio: ${c.name}</title>
</head>
<div id="header">
<h1>Portfolio for ${c.name}</h1>
<br>
<br></div>
${sidebar(['/p/'+str(c.id),'Portfolio'],c.listitems,['/portfolios/resume/'+str(c.id),'Resume'])}
<br>
<div id="main">
	<h3>${c.project}</h3>
%if c.project != 'No Project':
${h.tags.form('files')}
%if c.files:
<ul>
<%
x=False
%>
%for file in c.files:
	%if x: 
			${rowodd(file.filename,url(controller='view',action='file',id=c.id,project=c.project))}
			<%
      x=False
   		%>
  %else:
			${roweven(file.filename,url(controller='view',action='file',id=c.id, project=c.project))}
			<%
      x=True		
			%>
	%endif
%endfor
  </ul>
	%endif
</form>
%endif
</div>

</html>
