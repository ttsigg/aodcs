<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"><title>AODCS</title>



${h.tags.stylesheet_link('/styles/style.css')}

</head>
<body>
		<div id="header">
			<h1> AODCS</h1><br><br>
		</div>
		<br><br>
		
		<% messages = h.flash.pop_messages() %>
		% if messages:
		<ul id="flash-messages">
			% for message in messages:
			<li>${message}</li>
			% endfor
		</ul>
		% endif
	
		<div style="text-align: center; vertical-align: middle; postion: relative; top: -200px;">
		${h.tags.form("/login/submit", method="post")}
		Username: ${h.tags.text("username")}<br>
		Password: ${h.tags.password("password")}<br>
		${h.tags.submit("login","Login")}
		${h.tags.end_form()}

<!--		Usernames and passwords are:<br>
		<strong>student</strong> default<br>
		<strong>faculty</strong> default<br>
		<strong>admin</strong> default<br>
		-->

	</div>
</body>
</html>
