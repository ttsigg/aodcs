<%inherit file="/base.mako"/>
<%namespace file="functions.mako" import="rowodd,roweven"/>

<h3>Search Results</h3><br>
%if c.users:
<ul id='results'>
%for user in c.users:
	<li>${user.name}	
	${h.tags.link_to('edit',url=h.url(controller='prof',action='edit',id=user.username))}	
	${h.tags.link_to('delete',url=h.url(controller='admin',action='delete',id=user.username))}
	</li>
%endfor
	</ul>
%endif

