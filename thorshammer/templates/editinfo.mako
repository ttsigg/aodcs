<%inherit file='/base.mako'/>
<div id="main">
${h.tags.form(url(controller='prof',action='save',id=c.fid))}
  <h2>Edit User</h2>
  <br>
  <table>
    <tbody>
      <tr>
				<td>Username:</td>
				<td>${h.tags.text('username',value=c.fusername)}</td>
        <td>Name:</td>
        <td>${h.tags.text('name',value=c.fname)}</td>
      </tr>
      <tr>
        <td>Bio:</td>
        <td colspan=3>${h.tags.textarea('bio',cols=50, rows=8,content=c.fbio)}</textarea></td>
      </tr>
      <tr>
      <tr>
        <td>Password:</td>
        <td>${h.tags.password('pass1')}</td>    
        <td>Confirm Password:</td>
        <td>${h.tags.password('pass2')}</td>
      </tr>
			<tr>
			<td>Portfolio URL:</td>
			<td>${h.tags.link_to(h.url('/p/'+str(c.fport)),h.url('/p/'+str(c.fport)))}</td>
			</tr>
			<tr>
			<td>Role:</td>
			%if c.role == 'Administrator':
			<td>${h.tags.select('role',c.frole,c.froles)}</td>
			%else:
			<td>${c.frole[1]}</td>
			%endif
			
    </tbody>
  </table>
  <input value="Save" name="Save" type="submit">
	${h.tags.end_form()}
</div>


