<%inherit file="base.mako"/>

<div id="main">
	${h.tags.form('/faculty/savegrade/'+c.projectid, method="post")}
  <h2>Leave Feedback<br>
  </h2>
  <br>
  <table>
    <tbody>
      <tr>
        <td>Project:</td>
				<td colspan="3"><h2>${c.project.name}<h2></td>
      </tr>
      <tr>
        <td>Feedback:<br>
        </td>
				<td colspan="3">${h.tags.textarea('feedback', cols=51, rows=8)}</textarea></td>
      </tr>
      <tr>
        <td>Grade:</td>
        <td>
        ${h.tags.text('grade')}
        </td>
      </tr>
    </tbody>
  </table><br>
${h.tags.submit("Save","Save")}
${h.tags.end_form()}
</div>

</body></html>
