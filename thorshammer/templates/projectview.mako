<%inherit file="/base.mako"/>
<%namespace file="functions.mako" import="rowodd,roweven,sidebar"/>

${sidebar(['/project/view/default','Projects'],c.listitems,['/project/newproject','New Project'])}

<div id="main">
	<h3>${c.project}</h3>
%if c.project != 'No Project':
  <div id="controls"><button>Move</button><button onlick=alert("Are you sure?")>Delete</button>
	<button onclick=window.location.href="/project/upload/${c.projectid}">Upload</button>
	%if c.portid is not None:
	%if c.inportfolio is False:
	<button onclick=window.location.href="/portfolio/publish/${c.userid}?proj=${c.projectid}">Publish to Portfolio</button>
	%else:
	<button onclick=window.location.href="/portfolio/remove/${c.userid}?proj=${c.projectid}">Remove from Portfolio</button>
	%endif
	%else:
	<button onclick=window.location.href="/portfolio/publish/${c.userid}?proj=${c.projectid}">Create Portfolio</button>
	%endif
	</div>
${h.tags.form('files')}
%if c.files:
<ul>
<%
x=False
%>
%for file in c.files:
	%if x: 
			${rowodd(file.filename,url(controller='view',action='file',id=file.id))}
			<%
      x=False
   		%>
	 %else:
				${roweven(file.filename,url(controller='view',action='file',id=file.id))}
			<%
      x=True		
			%>
		%endif
%endfor
  </ul>
	%endif
</form>
%if c.grade is not None:
<div id="grade">
<strong>Grade: </strong>${c.grade}
</div>
<div id="comments">
<strong>Instructor comments:</strong> ${c.comments}
</div>
%endif
%endif
</div>
</body></html>
