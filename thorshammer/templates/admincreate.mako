<%inherit file='/base.mako'/>
<div id="main">
${h.tags.form(url(controller='admin',action='new'))}
  <h2>Create User</h2>
  <br>
  <table>
    <tbody>
      <tr>
				<td>Username:</td>
				<td>${h.tags.text('username')}</td>
        <td>Name:</td>
        <td>${h.tags.text('name')}</td>
      </tr>
      <tr>
        <td>Bio:</td>
        <td colspan=3>${h.tags.textarea('bio',cols=50, rows=8)}</textarea></td>
      </tr>
      <tr>
      <tr>
        <td>Password:</td>
        <td>${h.tags.password('pass1')}</td>    
        <td>Confirm Password:</td>
        <td>${h.tags.password('pass2')}</td>
      </tr>
			<tr>
			<td>Role:</td>
			<td>${h.tags.select('role','',c.roles)}</td>
    </tbody>
  </table>
  <input value="Save" name="Save" type="submit">
	${h.tags.end_form()}
</div>


