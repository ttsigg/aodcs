"""The base Controller API

Provides the BaseController class for subclassing.
"""
from pylons import request, response, session,tmpl_context as c,url
from pylons.controllers import WSGIController
from pylons.controllers.util import redirect
from pylons.templating import render_mako as render

from thorshammer.model.meta import Session

class BaseController(WSGIController):
    requires_auth = False
    role = 0

    def __before__(self):
        #auth stuff
        if self.requires_auth and ('logged_in' not in session or session['logged_in'] is False):
            session['path_before_login'] = request.path_info
            session.save()
            flash('Please login')
            return redirect(url(controller='login'))
        #session stuff
        if session and 'name' in session and 'role' in session:
            c.name = session['name']
            c.role = session['role']
            c.userid = session['userid']
        else:
            c.name = 'nope'
            c.role = 'nope'
            c.userid = 'nope'



    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        # WSGIController.__call__ dispatches to the Controller method
        # the request is routed to. This routing information is
        # available in environ['pylons.routes_dict']
        try:
            return WSGIController.__call__(self, environ, start_response)
        finally:
            Session.remove()
