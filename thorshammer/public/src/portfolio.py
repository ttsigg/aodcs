import logging

from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect

from thorshammer.lib.base import BaseController, render

log = logging.getLogger(__name__)

class PortfolioController(BaseController):

    def index(self):
        return 'This url isn\'t much use without a name at the end.'

    def view(self,id):
        # Return a rendered template
        return render('/portfolio.mako')
        # or, return a string
        #return 'Hello World'
    def add(self, id):
        return ('project added to portfolio view')
