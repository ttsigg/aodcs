"""The application's model objects"""
from thorshammer.model import meta
from thorshammer.model.meta import Session, Base
import sqlalchemy as sa
from sqlalchemy.orm import mapper
import hashlib

def init_model(engine):
    """Call me before using any of the tables or classes in the model"""
    meta.Session.configure(bind=engine)
    meta.engine = engine


#Base = declarative_base()
metadata = sa.MetaData()
users = sa.Table('users', metadata,
        sa.Column('id', sa.types.Integer, primary_key=True),
        sa.Column('username', sa.types.Unicode(20),nullable=False,unique=True,index=True),
        sa.Column('passwd', sa.types.Unicode(41)),
        sa.Column('name', sa.types.Unicode(30)),
        sa.Column('role_id', sa.types.Integer, sa.ForeignKey('roles.id')),
        sa.Column('bio', sa.types.UnicodeText()),
        sa.Column('portfolio_id', sa.types.Integer, sa.ForeignKey('portfolios.id')),
        )

projects = sa.Table('projects', metadata,
        sa.Column('id', sa.types.Integer, primary_key=True),
        sa.Column('name', sa.types.Unicode(25),index=True,nullable=False),
        sa.Column('desc', sa.types.UnicodeText),
        sa.Column('lang', sa.types.Unicode(20)),
        sa.Column('grade',sa.types.Integer),
        sa.Column('comments',sa.types.UnicodeText),
        sa.Column('course_id',sa.types.Integer,sa.ForeignKey('courses.id')),
        sa.Column('approved',sa.types.Integer),
        )

projects_users = sa.Table('projects_users', metadata,
        sa.Column('user_id', sa.types.Integer, sa.ForeignKey('users.id')),
        sa.Column('project_id', sa.types.Integer, sa.ForeignKey('projects.id')),
        )

roles = sa.Table('roles',metadata,
        sa.Column('id',sa.types.Integer,primary_key=True),
        sa.Column('name',sa.types.Unicode(20)),
        sa.Column('desc',sa.types.UnicodeText),
        )

files = sa.Table('files',metadata,
        sa.Column('id',sa.types.Integer,primary_key=True),
        sa.Column('filename',sa.types.Unicode(50)),
        sa.Column('docname',sa.types.Unicode(50)),
        sa.Column('project_id',sa.types.Integer,sa.ForeignKey('projects.id')),
        sa.Column('filetype',sa.types.Unicode(10)),
        sa.Column('comments',sa.types.UnicodeText),
        )

courses = sa.Table('courses',metadata,
        sa.Column('id',sa.types.Integer,primary_key=True),
        sa.Column('name',sa.types.Unicode(50)),
        sa.Column('number',sa.types.Unicode(10)),
        sa.Column('desc',sa.types.UnicodeText),
        sa.Column('instructor_id',sa.types.Integer,sa.ForeignKey('users.id')),
        )


portfolios = sa.Table('portfolios',metadata,
        sa.Column('id',sa.types.Integer, primary_key=True),
        sa.Column('resume',sa.types.Unicode(50)),
        )

projects_portfolios = sa.Table('projects_portfolios', metadata,
        sa.Column('project_id',sa.types.Integer,sa.ForeignKey('projects.id')),
        sa.Column('portfolio_id',sa.types.Integer,sa.ForeignKey('portfolios.id')),
        )

students_courses = sa.Table('students_courses',metadata,
        sa.Column('user_id',sa.types.Integer,sa.ForeignKey('users.id')),
        sa.Column('course_id',sa.types.Integer,sa.ForeignKey('courses.id')),
        )

class Course(object):
    def __init__(self,name,number):
        self.name=name
        self.number=number

class User(object):
    def __init__(self,username,passwd):
        self.username=username
        self.passwd=hashlib.sha1(passwd).hexdigest()
    def find(self,username):
        pass

class Project(object):
    def __init__(self,name,course):
        self.name = name
        self.course = course

class File(object):
    def __init__(self,filename,docname,project):
        self.filename = filename
        self.docname = docname
        self.project = project

class Role(object):
    def __init__(self,name):
        self.name = name

class Portfolio(object):
    def __init__(self):
       pass 




mapper(Project,projects,properties={
    'users':sa.orm.relationship(User,secondary=projects_users, backref='projects'),
    'files':sa.orm.relationship(File,backref='project'),
    'course':sa.orm.relationship(Course,backref='projects'),
    })

mapper(User,users,properties={
    'portfolio':sa.orm.relationship(Portfolio,uselist=False,backref='user'),
    'role':sa.orm.relationship(Role)
    })

mapper(Course,courses,properties={
    'instructor':sa.orm.relationship(User,uselist=False,backref='courses_taught'),
    'students':sa.orm.relationship(User,secondary=students_courses,backref='courses'),
    })

mapper(Portfolio,portfolios,properties={
    'projects':sa.orm.relationship(Project,secondary=projects_portfolios, backref='portfolios'),
    })

mapper(File,files)

mapper(Role,roles)

