"""Setup the thorshammer application"""
import thorshammer.model as model 
import logging

import pylons.test
#import sqlalchemy as sa
from thorshammer.config.environment import load_environment
from thorshammer.model.meta import Session, Base
from thorshammer.model import meta

log = logging.getLogger(__name__)

def setup_app(command, conf, vars):
    """Place any commands to setup thorshammer here"""
    # Don't reload the app if it was loaded under the testing environment
#    if not pylons.test.pylonsapp:
    config = load_environment(conf.global_conf, conf.local_conf)

    # Create the tables if they don't already exis

    model.metadata.drop_all(bind=meta.engine)
    model.metadata.create_all(bind=meta.engine)
    
    model.init_model(Session.bind)

    admrole = model.Role('Administrator')
    facrole = model.Role('Faculty')
    sturole = model.Role('Student')
    Session.add(admrole)
    Session.add(facrole)
    Session.add(sturole)



    admin = model.User('admin','default')    
    admin2 = model.User('admin2','default')
    faculty = model.User('faculty','default')
    faculty2 = model.User('faculty2','default')
    student = model.User('student','default')
    student2 = model.User('student2','default')
    student3 = model.User('student3','default')
    siggins = model.User('siggins','default')
    admin.role = admrole
    admin.name = 'Joe Admin'
    admin2.role = admrole
    admin2.name = 'Neo .'
    faculty.role = facrole
    faculty.name = 'Professor Roy'
    faculty2.role = facrole
    faculty2.name = 'Faculty Guy'
    student.role = sturole
    student.name = 'Some Student'
    student2.role = sturole
    student2.name = 'Saxton Hale'
    student3.role = sturole
    student3.name = 'Jim Bean'
    siggins.name = 'Tim Siggins'
    siggins.role = sturole
    
    
    course1 = model.Course('Studyology','CSCI 0001')
    course1.instructor=faculty
    course1.students=[student,student2,siggins]
    course1.desc='a fake course, bro'
    
    course2 = model.Course('Advanced Slacking','CSCI 0002')
    course2.instructor=faculty
    course2.students=[student,student2,siggins,student3]
    course2.desc='this course is challenging'

    course3 = model.Course('Sleeping','CSCI 5460')
    course3.instructor=faculty2
    course3.students=[student,student2,siggins]
    course3.desc='something'

    project1 = model.Project('Assignment1',course1)
    project1.desc='a fake project'

    project2 = model.Project('Assignment2',course2)
    project2.desc='a real project'
    
    project3 = model.Project('Battleship',course1)
    project3.desc = 'battleship the game'

    project4 = model.Project('DERP',course2)
    project4.desc = 'nope.avi'

    project5 = model.Project('AODCS',course3)
    project5.desc = 'Project 5'

    student.projects=[project1,project3]
    siggins.projects=[project1,project2,project4,project5]
    student2.projects=[project1,project2,project3]
    student3.projects=[project4]

    file1 = model.File('LinkedList.java','LinkedList.html',project1)
    file1.filetype='java'


    Session.add(admin)
    Session.add(admin2)
    Session.add(faculty)
    Session.add(faculty2)
    Session.add(student)
    Session.add(student2)
    Session.add(student3)
    Session.add(siggins)
    Session.add(course1)
    Session.add(course2)
    Session.add(course3)
    Session.add(project1)
    Session.add(project2)
    Session.add(project3)
    Session.add(project4)
    Session.add(project5)
    Session.add(file1)
    Session.commit()
    

