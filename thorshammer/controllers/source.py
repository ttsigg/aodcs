import logging
import webhelpers as h
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect

from thorshammer.lib.base import BaseController, render

log = logging.getLogger(__name__)

class SourceController(BaseController):

    def index(self):
        # Return a rendered template
        #return render('/source.mako')
        # or, return a string
        return render('/source.mako')

    def view(self,id):

        c.file=id
        f=open('/home/tim/thorshammer/src/'+c.file,'r')
        
        c.lang=id.split(".")[-1]

        c.content=f.read()

        return render('/source.mako')
