import logging
import os
import shutil
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from thorshammer import model
from thorshammer.lib.base import BaseController, render, Session
from thorshammer.lib.helpers import flash
log = logging.getLogger(__name__)

class FacultyController(BaseController):
    requires_auth = True
#    def __before__(self):
#        self.project_q = Session.query(model.Project)


    def view(self,id):
        c.name=session.get('name')
        userid = session.get('userid')
        user = Session.query(model.User).filter_by(id=userid).first()
#find courses taught by instructor
        courses = Session.query(model.Course).filter_by(instructor_id=userid).all()
        cids=[]
        for co in courses:
            cids.append(co.id)
        projects = Session.query(model.Project).filter(model.Project.course_id.in_(cids))
        
        
        if projects is None:
            c.project='No Project'
            c.projectid=0
            c.files=None
            c.students=None
            c.approved=-1
        else:

            proj = projects.filter_by(name=id).first()
    
            if proj is None:
                c.project='No Project'
                c.files=None
                c.students=None
                c.approved=-1
            else:
                c.project=proj.name
                c.projectid = proj.id
                c.approved = proj.approved
                c.files = Session.query(model.File).filter(model.File.project.has(id=proj.id)).all()
                c.students = Session.query(model.User).filter(model.User.projects.contains(proj)).all()


        c.listitems=[]
        if courses is not None:
            for course in courses:
                ps = []
                p2 = projects.filter(model.Project.course.has(id=course.id)).all()
                for p in p2:
                    ps.append([url(controller='faculty',action='view',id=p.name),p.name])
                
                 
                c.listitems.append([course.number,ps])
        else:
            c.listlitems.append(['None','None'])

        return render('facultycp.mako')

    def newcourse(self):
        c.name=session.get('name')
        c.userid=session.get('userid')
        #populate users from database
        users = Session.query(model.User).filter_by(role_id=3).all()
        c.users=[]
        for user in users:
            c.users.append([user.id,user.name])
        
        
        #render the new project page
        return render('newcourse.mako')

    def create(self):
        name=unicode(request.params['name'])
        number=unicode(request.params['number'])
        desc=unicode(request.params['desc'])
        
        inst = Session.query(model.User).filter_by(id=session.get('userid')).first()


        userids = []
        for u in request.params.getall('users'):
            userids.append(int(u))
        course = model.Course(name,number)
        course.instructor = inst
        course.students = Session.query(model.User).filter(model.User.id.in_(userids)).all()
        course.desc=desc
        Session.add(course)
        Session.commit()
        
        
        redirect(url(controller='faculty',action='view',id=name))

    def approve(self,id):
        projectid = id
        p = Session.query(model.Project).filter_by(id=id).first()
        if (p.approved == 1):
            p.approved = 0
            flash('Project Revoked!')
        elif (p.approved == 0):
            p.approved = 1 
            flash('Project Approved!')
        Session.commit() 
        
        redirect(url(controller='faculty',action='view',id=p.name))


    def grade(self,id):
        c.projectid = id
        c.project = Session.query(model.Project).filter_by(id=id).first()
        c.name=session.get('name')
        return render('grade.mako')

    def savegrade(self,id):
        project = id
        feedback = request.params['feedback']
        grade = request.params['grade']

        proj = Session.query(model.Project).filter_by(id=id).first()
        proj.grade = grade
        proj.comments = feedback
        
        Session.commit()


        redirect(url(controller='faculty',action='view',id=id))


