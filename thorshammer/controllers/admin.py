import logging
import hashlib
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from thorshammer import model
from thorshammer.lib.base import BaseController, render,Session
from thorshammer.lib.helpers import flash
log = logging.getLogger(__name__)

class AdminController(BaseController):
    requires_auth=True
    requires_role=2
    def index(self):
        return render('admincp.mako')
    
    def search(self):
        target = unicode(request.params['derp'])
        print target
        c.users = Session.query(model.User).filter(model.User.name.contains((target)))
        print c.users
        return render('adminfind.mako')

    def create(self):
        roles = Session.query(model.Role)       
        c.roles=[]
        for role in roles:
            c.roles.append([role.id,role.name])
        return render('admincreate.mako')

    def new(self):
        if (unicode(request.params['pass1']) != unicode(request.params['pass2'])):
            flash('bad password')
            redirect(url(controller='admin',action='create'))
        uname = request.params['username']
        #db_user = Session.query(model.User).filter_by(username=uname)
        #if db_user is not None:
        #    print db_user
        #    flash('user exists')
        #    redirect(url(controller='admin',action='create'))
        
        password = unicode(request.params['pass1'])
        
        user = model.User(uname,password)
        user.name=request.params['name']
        user.bio=request.params['bio']
        print request.params['role']
        role = Session.query(model.Role).filter_by(id=request.params['role']).first()
         
        user.role=role
        Session.add(user)
        Session.commit()
        flash('success')
        redirect(url(controller='admin'))

    def edit(self,id):
        db_user = Session.query(model.User).filter_by(username=id).first()
        if db_user is None:
           db_user = Session.query(model.User).filter_by(id=id).first()
        if db_user is None:
           flash('bad user')
           redirect(url(controller='admin'))
        c.fusername = db_user.username
        c.fname = db_user.name
        c.fbio = db_user.bio
        c.frole = [db_user.role.id,db_user.role.name]
        roles = Session.query(model.Role)
        c.fid = db_user.id
        c.froles=[]
        for role in roles:
            c.froles.append([role.id,role.name])
        return render('adminedit.mako')

    def save(self,id):
        db_user = Session.query(model.User).filter_by(id=id).first()
        db_user.username = unicode(request.params['username'])
        db_user.name = unicode(request.params['name'])
        db_user.bio = unicode(request.params['bio'])
        if (unicode(request.params['pass1']) != ''  and unicode(request.params['pass1']) == unicode(request.params['pass2'])):
            db_user.passwd = hashlib.sha1(str(request.params['pass1'])).hexdigest()
        elif (unicode(request.params['pass1']) != ''):
            flash('passwords do not match')
            redirect(url(controller='admin',action='edit',id=id))
        else:
            pass
       
        db_user.role = Session.query(model.Role).filter_by(id=request.params['role']).first()
        Session.commit()

        flash('success')
        redirect(url(controller='admin',action='edit',id=id))
