import logging
import os
import shutil
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from thorshammer import model
from thorshammer.lib.base import BaseController, render, Session
from thorshammer.lib.helpers import flash


log = logging.getLogger(__name__)

class ProjectController(BaseController):
    requires_auth = True
#    def __before__(self):
#        self.project_q = Session.query(model.Project)


    def view(self,id):
        c.name=session.get('name')
        userid = session.get('userid')
        c.userid=userid
        user = Session.query(model.User).filter_by(id=userid).first()
        projects = Session.query(model.Project).filter(model.Project.users.any(id=userid))
        courses = Session.query(model.Course).filter(model.Course.students.any(id=userid)).all()
        c.portid = user.portfolio_id

        if projects.all() is None:
            c.inportfolio=False
            c.project='No Project'
            c.projectid=0
            c.files=None
            c.projects=None
            c.grade=None
            c.comments=None
        else:
            proj = projects.filter_by(name=id).first()
    
            if proj is None:
                c.project='No Project'
                c.files=None
                c.projectid=0
                c.grade=None
                c.comments=None
                c.inportfolio=False
            else:
                c.project=proj.name
                c.projectid = proj.id
                c.grade = proj.grade
                c.comments = proj.comments
                c.files = Session.query(model.File).filter(model.File.project.has(id=proj.id)).all()
                if (c.portid != 0):
                    c.inportfolio = len(Session.query(model.Portfolio).filter_by(id=c.portid).filter(model.Portfolio.projects.any(id=proj.id)).all()) != 0
                else:
                    c.inportfolio=False

        c.listitems=[]
        if courses is not None:
            for course in courses:
                ps = []
                p2 = projects.filter(model.Project.course.has(id=course.id)).all()
                for p in p2:
                    ps.append([url(controller='project',action='view',id=p.name),p.name])
                
                 
                c.listitems.append([course.number,ps])
        else:
            c.listlitems.append(['None','None'])

        return render('projectview.mako')

    def newproject(self):
        c.name=session.get('name')
        c.userid=session.get('userid')
        #populate users from database
        users = Session.query(model.User).filter_by(role_id=3).all()
        c.users=[]
        for user in users:
            c.users.append([user.id,user.name])
        
        #populate courses from the database
        courses = Session.query(model.Course).filter(model.Course.students.any(id=c.userid)).all()
        c.courses = []
        for course in courses:
            c.courses.append([course.id,course.number])

        #render the new project page
        return render('newproject.mako')

    def create(self):
        name=unicode(request.params['name'])
        desc=unicode(request.params['desc'])
        lang='unimplemented'
        courseid=int(request.params['course'])
        
        userids = []
        for u in request.params.getall('users'):
            print(u)
            userids.append(int(u))
        course = Session.query(model.Course).filter_by(id=courseid).first()
        proj = model.Project(name,course)
        
        userids
        proj.users = Session.query(model.User).filter(model.User.id.in_(userids)).all()
        proj.desc=desc
        proj.lang=lang
        Session.add(proj)
        Session.commit()
        
        
        redirect(url(controller='project',action='view',id=name))


    def upload(self,id):
        c.projectid = id
        c.project = Session.query(model.Project).filter_by(id=id).first().name
        c.name=session.get('name')
        return render('upload.mako')

    def recieve(self,id):
        pid = id
        project = Session.query(model.Project).filter_by(id=id).first()
        src = request.params['src']
        doc = request.params['doc']
        srcname = src.filename.lstrip(os.sep)
        docname = doc.filename.lstrip(os.sep)
        longsrc = os.path.join('/home/tim/uploads',pid,'src',src.filename.lstrip(os.sep))
        longdoc = os.path.join('/home/tim/uploads',pid,'doc',doc.filename.lstrip(os.sep))



        if not os.path.exists(os.path.dirname(longsrc)):
            os.makedirs(os.path.dirname(longsrc))
        if not os.path.exists(os.path.dirname(longdoc)):
            os.makedirs(os.path.dirname(longdoc))


        permasrc = open(os.path.join(longsrc),'w')
        permadoc = open(os.path.join(longdoc),'w')

        shutil.copyfileobj(src.file,permasrc)
        permasrc.close()

        shutil.copyfileobj(doc.file,permadoc)
        permadoc.close()
        
        codetype = srcname.split('.')[-1]
        
        f = model.File(unicode(srcname),unicode(docname),project)
        f.filetype=codetype
        Session.add(f)
        Session.commit()

        redirect(url(controller='project',action='view',id=id))


    def edit(self,id):
        c.project = id

        return render('/project.mako')


    def delete(self,id):

        return('Deleted')
