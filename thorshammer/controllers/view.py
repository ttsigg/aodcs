import logging
import os
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect

from thorshammer.lib.base import BaseController, render,Session
from thorshammer import model


log = logging.getLogger(__name__)

class ViewController(BaseController):

    def index(self):
        # Return a rendered template
        #return render('/view.mako')
        # or, return a string
        return render('/split.mako')

    def file(self,id):
        c.name=session.get('user')
        user = Session.query(model.User).filter_by(id=session.get('userid')).first()
        db_file = Session.query(model.File).filter_by(id=id).first()
        
                
        proj=Session.query(model.Project).filter_by(id=db_file.project_id).first()
        c.projectid = proj.id
        
        c.file=db_file.filename
        c.fileid=db_file.id
        c.docsrc=db_file.docname
        f=open(os.path.join('/home/tim/uploads/',str(c.projectid),'src',c.file),'r')
        c.lang=c.file.split(".")[-1]
        c.content=f.read()



        files=Session.query(model.File).filter(model.File.project.has(id=c.projectid)).all()
        c.listitems=[]
        if files is not None:
            for f in files:
                c.listitems.append([url(controller='view',action='file',id=f.id),f.filename])

        else:
            c.listlitems.append(['None','None'])

        return render ('/split.mako')

    def doc(self,id):
        file_db = Session.query(model.File).filter_by(id=id).first()
        f=open(os.path.join('/home/tim/uploads/',str(file_db.project_id),'doc',file_db.docname),'r')

        return f.read()

