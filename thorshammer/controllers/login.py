import logging
import hashlib
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from thorshammer import model
from thorshammer.lib.base import BaseController, render, Session
from thorshammer.lib.helpers import flash


log = logging.getLogger(__name__)

class LoginController(BaseController):


#    def __before__(self):
 #       self.user_q = Session.query(model.User)


    def index(self):
        # Return a rendered template
        #return render('/login.mako')
        # or, return a string
        return render('/login.mako')

    def submit(self):
        form_username = str(request.params['username'])
        form_password = str(request.params['password'])


        db_user = Session.query(model.User).filter_by(username=form_username,passwd=(hashlib.sha1(form_password).hexdigest())).first()

        if (db_user is None):
            msg = 'Invalid username or password'
            destination = url('/login')
            flash(msg)
            redirect(destination)


        session['user'] = form_username
        session['name'] = db_user.name
        session['role'] = db_user.role.name
        session['userid'] = db_user.id
        session['logged_in'] = True
        session.save()
        msg = 'logged in'
        print db_user.role.name
        print db_user.role.id
        if db_user.role.id == 1:
            destination = url(controller='admin',action='index')
        elif session.get('path_before_login'):
            destination = url(session.get('path_before_login'))
        elif db_user.role.id == 2:
            destination=url(controller='faculty',action='view',id='default')
        else:
            destination = url(controller='project', action='view', id='default')
        flash(msg)
        redirect(destination)


    def logout(self):

        session.clear()
        session.save()
        flash('Logged out')
        redirect(url('/login'))
        
