import logging
import os
import shutil
from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from thorshammer import model
from thorshammer.lib.base import BaseController, render, Session
from thorshammer.lib.helpers import flash


log = logging.getLogger(__name__)

class PortfolioController(BaseController):

    def index(self):
        return 'This url isn\'t much use without a name at the end.'

    def view(self,id,p='t'):
        if p == 't':
            project=unicode(request.params['project'])
        else:
            project=None
        port=Session.query(model.Portfolio).filter_by(id=id).first() 
        user=Session.query(model.User).filter(model.User.portfolio.has(id=id)).first()
        if(user is None):
            return 'Portfolio does not exist'
        
        c.name = user.name
        c.id = id
        projects = Session.query(model.Project).filter(model.Project.portfolios.any(id=id))
        pr = projects.all()
        if pr is None:
            c.project='No Project'
            c.files=None
            c.projects=None
        else:
            if project is not None:
                proj = projects.filter_by(name=project).first()
            else:
                proj = None
            if proj is None:
                c.project='No Project'
                c.files=None
                c.projectid=0
            else:
                c.project=proj.name
                c.projectid=proj.id
                c.files = Session.query(model.File).filter(model.File.project.has(id=proj.id)).all()

        c.listitems=[]

        print(projects)
        if pr is not None:
            for p in pr:
                if p.approved is not None and p.approved == 1:
                    c.listitems.append([url(controller='portfolio',action='view',id=c.id,project=p.name),p.name])
                
                
        else:
            c.listlitems.append(['None','None'])
        
        
        
        return render('/portfolio.mako')




    def publish(self, id):
        pid=request.params['proj']
        u = Session.query(model.User).filter_by(id=id).first()
        #print('user='+str(u.id))
        if u.portfolio_id is None:
            n=True
            #print('\n\nderp1\n\n\n')
            portf = model.Portfolio()
            u.portfolio=portf
        else:
            n=False
            #print('\n\nderp2\nport'+u.portfolio_id+'\n'+pid+'\n')
            portf = Session.query(model.Portfolio).filter_by(id=u.portfolio_id).first()
        
        proj = Session.query(model.Project).filter_by(id=pid).first()
        if (proj.approved is None):
            proj.approved = 0
        portf.projects.append(proj)

        Session.commit()
        #print('GOD DAMMIT FUCK!!!!!!!!!!!!!\n\n\n\n\n!!!!!!!!')
        if n:
            flash('Portfolio successfully created')
        else:
            flash('Project added to portfolio view')
        
        redirect(url(controller='project',action='view',id=id))

    def remove(self, id):
        pid=request.params['proj']
        u = Session.query(model.User).filter_by(id=id).first()
        #print('user='+str(u.id))
        if u.portfolio_id is None:
            #print('\n\nderp1\n\n\n')
            portf = model.Portfolio()
            u.portfolio=portf
        else:
            #print('\n\nderp2\nport'+u.portfolio_id+'\n'+pid+'\n')
            portf = Session.query(model.Portfolio).filter_by(id=u.portfolio_id).first()
        
        proj = Session.query(model.Project).filter_by(id=pid).first()

        portf.projects.remove(proj)

        Session.commit()
        #print('GOD DAMMIT FUCK!!!!!!!!!!!!!\n\n\n\n\n!!!!!!!!')
            
        flash ('Project removed from portfolio view')

        redirect(url(controller='project',action='view',id=id))
