# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1288194097.151688
_template_filename='/home/tim/school/thorshammer/thorshammer/templates/settingsf.mako'
_template_uri='/settingsf.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n<div id="main">\n<form method="post" action="save" name="Options">\n  <h2>Settings</h2>\n  <br>\n  <table>\n    <tbody>\n      <tr>\n        <td>Name:</td>\n        <td><input name="Name"></td>\n      </tr>\n      <tr>\n        <td>Bio:</td>\n        <td><textarea cols="50" rows="8" name="Bio"></textarea></td>\n      </tr>\n      <tr>\n        <td style="vertical-align: top;">Portfolio URL:<br>\n        </td>\n        <td style="vertical-align: top;">http://www.thorshammer.com/<input name="URL" value="siggins"><br>\n        </td>\n      </tr>\n      <tr>\n        <td style="vertical-align: top;">Custom Stylesheet:<br>\n        </td>\n        <td style="vertical-align: top;"><input name="CSS" type="file"><br>\n        </td>\n      </tr>\n      <tr>\n        <td>Password:</td>\n        <td><input name="pass1" type="password"></td>\n      </tr>\n      <tr>\n        <td>Confirm Password:</td>\n        <td><input name="pass2" type="password"></td>\n      </tr>\n    </tbody>\n  </table>\n  <input value="Save" name="Save" type="submit">\n</form>\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


