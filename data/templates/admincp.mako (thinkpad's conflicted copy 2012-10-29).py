# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349918663.367527
_enable_loop = True
_template_filename = '/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/admincp.mako'
_template_uri = 'admincp.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    # SOURCE LINE 2
    ns = runtime.TemplateNamespace('__anon_0x7f41780a73d0', context._clean_inheritance_tokens(), templateuri=u'functions.mako', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7f41780a73d0')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7f41780a73d0')._populate(_import_ns, [u'rowodd', u'roweven'])
        h = _import_ns.get('h', context.get('h', UNDEFINED))
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n')
        # SOURCE LINE 2
        __M_writer(u'\n\n\n<div id="main">\n<h3>Admin Control Panel</h3>\n')
        # SOURCE LINE 7
        __M_writer(escape(h.tags.form(h.url(controller='admin',action='search'))))
        __M_writer(u'\nFind Student:\n')
        # SOURCE LINE 9
        __M_writer(escape(h.tags.text("derp")))
        __M_writer(u'\n')
        # SOURCE LINE 10
        __M_writer(escape(h.tags.submit('Search','Search')))
        __M_writer(u'\n')
        # SOURCE LINE 11
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n')
        # SOURCE LINE 12
        __M_writer(escape(h.tools.button_to('Create User',h.url(controller='admin',action='create'))))
        __M_writer(u'\n')
        # SOURCE LINE 13
        __M_writer(escape(h.tags.form(h.url(controller='admin',action='search'))))
        __M_writer(u'\n')
        # SOURCE LINE 14
        __M_writer(escape(h.tags.hidden('derp')))
        __M_writer(u'\n')
        # SOURCE LINE 15
        __M_writer(escape(h.tags.submit('Display All Users','Display All Users')))
        __M_writer(u'\n')
        # SOURCE LINE 16
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


