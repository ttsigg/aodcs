# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349918710.796179
_enable_loop = True
_template_filename = u'/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/source.mako'
_template_uri = u'/source.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"\n  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">\n<html><head>\n\t\t')
        # SOURCE LINE 4
        __M_writer(escape(h.tags.javascript_link('/scripts/jquery-1.4.3.js')))
        __M_writer(u'\n\t\t')
        # SOURCE LINE 5
        __M_writer(escape(h.tags.javascript_link('/scripts/shCore.js')))
        __M_writer(u' \n\t\t')
        # SOURCE LINE 6
        __M_writer(escape(h.tags.javascript_link('/scripts/shAutoloader.js')))
        __M_writer(u'\n\n  <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">\n  <title>source</title>\n\n\n')
        # SOURCE LINE 12
        __M_writer(escape(h.tags.stylesheet_link('/styles/shCore.css')))
        __M_writer(u'\n')
        # SOURCE LINE 13
        __M_writer(escape(h.tags.stylesheet_link('/styles/shThemeDefault.css')))
        __M_writer(u'\n\n\n</head><body >\n<pre class="brush: ')
        # SOURCE LINE 17
        __M_writer(escape(c.lang))
        __M_writer(u'; toolbar: false;">\t\n\t')
        # SOURCE LINE 18
        __M_writer(escape(c.content))
        __M_writer(u'\n\t</pre>\n<script type="text/javascript">\n\nfunction path()\n{\n  var args = arguments,\n      result = []\n      ;\n       \n  for(var i = 0; i < args.length; i++)\n      result.push(args[i].replace(\'@\', \'/scripts/\'));\n       \n  return result\n};\n\t\tSyntaxHighlighter.autoloader.apply(null, path(\n  \'applescript            @shBrushAppleScript.js\',\n  \'actionscript3 as3      @shBrushAS3.js\',\n  \'bash shell             @shBrushBash.js\',\n  \'coldfusion cf          @shBrushColdFusion.js\',\n  \'cpp c                  @shBrushCpp.js\',\n  \'c# c-sharp csharp      @shBrushCSharp.js\',\n  \'css                    @shBrushCss.js\',\n  \'delphi pascal          @shBrushDelphi.js\',\n  \'diff patch pas         @shBrushDiff.js\',\n  \'erl erlang             @shBrushErlang.js\',\n  \'groovy                 @shBrushGroovy.js\',\n  \'java                   @shBrushJava.js\',\n  \'jfx javafx             @shBrushJavaFX.js\',\n  \'js jscript javascript  @shBrushJScript.js\',\n  \'perl pl                @shBrushPerl.js\',\n  \'php                    @shBrushPhp.js\',\n  \'text plain             @shBrushPlain.js\',\n  \'py python              @shBrushPython.js\',\n  \'ruby rails ror rb      @shBrushRuby.js\',\n  \'sass scss              @shBrushSass.js\',\n  \'scala                  @shBrushScala.js\',\n  \'sql                    @shBrushSql.js\',\n  \'vb vbnet               @shBrushVb.js\',\n  \'xml xhtml xslt html    @shBrushXml.js\'\n));\n     SyntaxHighlighter.all();\n</script>\n<!--</body></html>-->\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


