# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349918812.218588
_enable_loop = True
_template_filename = '/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/grade.mako'
_template_uri = 'grade.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'base.mako', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n\n<div id="main">\n\t')
        # SOURCE LINE 4
        __M_writer(escape(h.tags.form('/faculty/savegrade/'+c.projectid, method="post")))
        __M_writer(u'\n  <h2>Leave Feedback<br>\n  </h2>\n  <br>\n  <table>\n    <tbody>\n      <tr>\n        <td>Project:</td>\n\t\t\t\t<td colspan="3"><h2>')
        # SOURCE LINE 12
        __M_writer(escape(c.project.name))
        __M_writer(u'<h2></td>\n      </tr>\n      <tr>\n        <td>Feedback:<br>\n        </td>\n\t\t\t\t<td colspan="3">')
        # SOURCE LINE 17
        __M_writer(escape(h.tags.textarea('feedback', cols=51, rows=8)))
        __M_writer(u'</textarea></td>\n      </tr>\n      <tr>\n        <td>Grade:</td>\n        <td>\n        ')
        # SOURCE LINE 22
        __M_writer(escape(h.tags.text('grade')))
        __M_writer(u'\n        </td>\n      </tr>\n    </tbody>\n  </table><br>\n')
        # SOURCE LINE 27
        __M_writer(escape(h.tags.submit("Save","Save")))
        __M_writer(u'\n')
        # SOURCE LINE 28
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n</div>\n\n</body></html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


