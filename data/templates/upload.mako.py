# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1291599648.476638
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/upload.mako'
_template_uri='upload.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n<div id="main">\n')
        # SOURCE LINE 3
        __M_writer(escape(h.tags.form("/project/recieve/"+c.projectid,multipart=True)))
        __M_writer(u'\n  <h2>Upload for ')
        # SOURCE LINE 4
        __M_writer(escape(c.project))
        __M_writer(u'</h2>\n  <br>\n\t')
        # SOURCE LINE 6
        __M_writer(escape(h.tags.file('src')))
        __M_writer(u'\n\n\t')
        # SOURCE LINE 8
        __M_writer(escape(h.tags.file('doc')))
        __M_writer(u'\n\t')
        # SOURCE LINE 9
        __M_writer(escape(h.tags.hidden('projectid',value=c.projectid)))
        __M_writer(u'\n\t<br>\n\t')
        # SOURCE LINE 11
        __M_writer(escape(h.tags.submit('Files Away!','submit')))
        __M_writer(u'\n')
        # SOURCE LINE 12
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


