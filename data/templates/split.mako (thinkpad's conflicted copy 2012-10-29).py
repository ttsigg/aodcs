# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349917667.101826
_enable_loop = True
_template_filename = '/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/split.mako'
_template_uri = '/split.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    # SOURCE LINE 2
    ns = runtime.TemplateNamespace('__anon_0x7f228c0c1410', context._clean_inheritance_tokens(), templateuri=u'functions.mako', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7f228c0c1410')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7f228c0c1410')._populate(_import_ns, [u'rowodd', u'roweven', u'sidebar'])
        h = _import_ns.get('h', context.get('h', UNDEFINED))
        c = _import_ns.get('c', context.get('c', UNDEFINED))
        sidebar = _import_ns.get('sidebar', context.get('sidebar', UNDEFINED))
        str = _import_ns.get('str', context.get('str', UNDEFINED))
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n')
        # SOURCE LINE 2
        __M_writer(u'\n')
        # SOURCE LINE 3
        __M_writer(escape(h.tags.javascript_link('/scripts/jquery-1.4.3.js')))
        __M_writer(u'\n')
        # SOURCE LINE 4
        __M_writer(escape(h.tags.javascript_link('/scripts/shCore.js')))
        __M_writer(u'\n')
        # SOURCE LINE 5
        __M_writer(escape(h.tags.javascript_link('/scripts/shAutoloader.js')))
        __M_writer(u'\n\n')
        # SOURCE LINE 7
        __M_writer(escape(h.tags.stylesheet_link('/styles/shCore.css')))
        __M_writer(u'\n')
        # SOURCE LINE 8
        __M_writer(escape(h.tags.stylesheet_link('/styles/shThemeDefault.css')))
        __M_writer(u'\n\n')
        # SOURCE LINE 10
        __M_writer(escape(sidebar(['/view/file/'+str(c.fileid),'Files'],c.listitems,['/project/view/'+str(c.projectid),'Back'])))
        __M_writer(u'\n\n\n\n<div id="src" class="syntaxhighlighter"> ')
        # SOURCE LINE 14
        runtime._include_file(context, u'source.mako', _template_uri)
        __M_writer(u'</div><br>\n<div id=docs><iframe class="docs" src="/view/doc/')
        # SOURCE LINE 15
        __M_writer(escape(c.fileid))
        __M_writer(u'"></iframe></div>\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


