# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1292366368.53237
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/admincreate.mako'
_template_uri='admincreate.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        url = context.get('url', UNDEFINED)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n<div id="main">\n')
        # SOURCE LINE 3
        __M_writer(escape(h.tags.form(url(controller='admin',action='new'))))
        __M_writer(u'\n  <h2>Create User</h2>\n  <br>\n  <table>\n    <tbody>\n      <tr>\n\t\t\t\t<td>Username:</td>\n\t\t\t\t<td>')
        # SOURCE LINE 10
        __M_writer(escape(h.tags.text('username')))
        __M_writer(u'</td>\n        <td>Name:</td>\n        <td>')
        # SOURCE LINE 12
        __M_writer(escape(h.tags.text('name')))
        __M_writer(u'</td>\n      </tr>\n      <tr>\n        <td>Bio:</td>\n        <td colspan=3>')
        # SOURCE LINE 16
        __M_writer(escape(h.tags.textarea('bio',cols=50, rows=8)))
        __M_writer(u'</textarea></td>\n      </tr>\n      <tr>\n      <tr>\n        <td>Password:</td>\n        <td>')
        # SOURCE LINE 21
        __M_writer(escape(h.tags.password('pass1')))
        __M_writer(u'</td>    \n        <td>Confirm Password:</td>\n        <td>')
        # SOURCE LINE 23
        __M_writer(escape(h.tags.password('pass2')))
        __M_writer(u'</td>\n      </tr>\n\t\t\t<tr>\n\t\t\t<td>Role:</td>\n\t\t\t<td>')
        # SOURCE LINE 27
        __M_writer(escape(h.tags.select('role','',c.roles)))
        __M_writer(u'</td>\n    </tbody>\n  </table>\n  <input value="Save" name="Save" type="submit">\n\t')
        # SOURCE LINE 31
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


