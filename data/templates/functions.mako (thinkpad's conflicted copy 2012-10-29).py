# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349917667.155943
_enable_loop = True
_template_filename = u'/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/functions.mako'
_template_uri = u'/functions.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = ['sidebar', 'list', 'rowodd', 'roweven']


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        __M_writer = context.writer()
        # SOURCE LINE 3
        __M_writer(u'\n\n')
        # SOURCE LINE 7
        __M_writer(u'\n\n')
        # SOURCE LINE 28
        __M_writer(u'\n\n')
        # SOURCE LINE 47
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_sidebar(context,title,items,end):
    __M_caller = context.caller_stack._push_frame()
    try:
        h = context.get('h', UNDEFINED)
        def list(items):
            return render_list(context,items)
        string = context.get('string', UNDEFINED)
        type = context.get('type', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 9
        __M_writer(u'\n<div id="sidebar"><strong>Navigation</strong>\n<li> ')
        # SOURCE LINE 11
        __M_writer(escape(h.tags.link_to(title[1],title[0])))
        __M_writer(u' </li>\n\n')
        # SOURCE LINE 13
        __M_writer(escape(list(items)))
        __M_writer(u'\n')
        # SOURCE LINE 14
        if type(end) is string:
            # SOURCE LINE 15


            endtitle = 'new' 
            finallink = end
            
            
            # SOURCE LINE 19
            __M_writer(u'\n')
            # SOURCE LINE 20
        else:
            # SOURCE LINE 21

            endtitle = end[1]
            finallink = end[0]
            
            
            # SOURCE LINE 24
            __M_writer(u'\n')
        # SOURCE LINE 26
        __M_writer(escape(h.tags.link_to(endtitle,finallink)))
        __M_writer(u'\n</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_list(context,items):
    __M_caller = context.caller_stack._push_frame()
    try:
        type = context.get('type', UNDEFINED)
        def list(items):
            return render_list(context,items)
        unicode = context.get('unicode', UNDEFINED)
        h = context.get('h', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 30
        __M_writer(u'\n\n')
        # SOURCE LINE 32
        if items is not None:
            # SOURCE LINE 33
            __M_writer(u'<ul>\n')
            # SOURCE LINE 34
            for item in items:
                # SOURCE LINE 35
                if item is not None:
                    # SOURCE LINE 36
                    if type(item[1]) is unicode:
                        # SOURCE LINE 37
                        __M_writer(u'<li> ')
                        __M_writer(escape(h.tags.link_to(item[1],item[0])))
                        __M_writer(u'</li>\n')
                        # SOURCE LINE 38
                    else:
                        # SOURCE LINE 39
                        __M_writer(u'<li>')
                        __M_writer(escape(item[0]))
                        __M_writer(u'</li>\n\t')
                        # SOURCE LINE 40
                        __M_writer(escape(list(item[1])))
                        __M_writer(u'\n\n')
            # SOURCE LINE 45
            __M_writer(u'</ul>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_rowodd(context,title,link):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n  <li class="listodd"><input type="checkbox"><a href="')
        # SOURCE LINE 2
        __M_writer(escape(link))
        __M_writer(u'">')
        __M_writer(escape(title))
        __M_writer(u'</a></li>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_roweven(context,title,link):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        # SOURCE LINE 5
        __M_writer(u'\n  <li class="listodd"><input type="checkbox"><a href="')
        # SOURCE LINE 6
        __M_writer(escape(link))
        __M_writer(u'">')
        __M_writer(escape(title))
        __M_writer(u'</a></li>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


