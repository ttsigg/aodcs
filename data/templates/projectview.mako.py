# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1291748151.006905
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/projectview.mako'
_template_uri='projectview.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    # SOURCE LINE 2
    ns = runtime.Namespace('__anon_0x1aad290', context._clean_inheritance_tokens(), templateuri=u'functions.mako', callables=None, calling_uri=_template_uri, module=None)
    context.namespaces[(__name__, '__anon_0x1aad290')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x1aad290')._populate(_import_ns, [u'rowodd', u'roweven', u'sidebar'])
        c = _import_ns.get('c', context.get('c', UNDEFINED))
        url = _import_ns.get('url', context.get('url', UNDEFINED))
        h = _import_ns.get('h', context.get('h', UNDEFINED))
        roweven = _import_ns.get('roweven', context.get('roweven', UNDEFINED))
        sidebar = _import_ns.get('sidebar', context.get('sidebar', UNDEFINED))
        rowodd = _import_ns.get('rowodd', context.get('rowodd', UNDEFINED))
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n')
        # SOURCE LINE 2
        __M_writer(u'\n\n')
        # SOURCE LINE 4
        __M_writer(escape(sidebar(['/project/view/default','Projects'],c.listitems,['/project/newproject','New Project'])))
        __M_writer(u'\n\n<div id="main">\n\t<h3>')
        # SOURCE LINE 7
        __M_writer(escape(c.project))
        __M_writer(u'</h3>\n')
        # SOURCE LINE 8
        if c.project != 'No Project':
            # SOURCE LINE 9
            __M_writer(u'  <div id="controls"><button>Move</button><button onlick=alert("Are you sure?")>Delete</button>\n\t<button onclick=window.location.href="/project/upload/')
            # SOURCE LINE 10
            __M_writer(escape(c.projectid))
            __M_writer(u'">Upload</button>\n')
            # SOURCE LINE 11
            if c.portid is not None:
                # SOURCE LINE 12
                if c.inportfolio is False:
                    # SOURCE LINE 13
                    __M_writer(u'\t<button onclick=window.location.href="/portfolio/publish/')
                    __M_writer(escape(c.userid))
                    __M_writer(u'?proj=')
                    __M_writer(escape(c.projectid))
                    __M_writer(u'">Publish to Portfolio</button>\n')
                    # SOURCE LINE 14
                else:
                    # SOURCE LINE 15
                    __M_writer(u'\t<button onclick=window.location.href="/portfolio/remove/')
                    __M_writer(escape(c.userid))
                    __M_writer(u'?proj=')
                    __M_writer(escape(c.projectid))
                    __M_writer(u'">Remove from Portfolio</button>\n')
                    pass
                # SOURCE LINE 17
            else:
                # SOURCE LINE 18
                __M_writer(u'\t<button onclick=window.location.href="/portfolio/publish/')
                __M_writer(escape(c.userid))
                __M_writer(u'?proj=')
                __M_writer(escape(c.projectid))
                __M_writer(u'">Create Portfolio</button>\n')
                pass
            # SOURCE LINE 20
            __M_writer(u'\t</div>\n')
            # SOURCE LINE 21
            __M_writer(escape(h.tags.form('files')))
            __M_writer(u'\n')
            # SOURCE LINE 22
            if c.files:
                # SOURCE LINE 23
                __M_writer(u'<ul>\n')
                # SOURCE LINE 24

                x=False
                
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                # SOURCE LINE 26
                __M_writer(u'\n')
                # SOURCE LINE 27
                for file in c.files:
                    # SOURCE LINE 28
                    if x: 
                        # SOURCE LINE 29
                        __M_writer(u'\t\t\t')
                        __M_writer(escape(rowodd(file.filename,url(controller='view',action='file',id=file.id))))
                        __M_writer(u'\n\t\t\t')
                        # SOURCE LINE 30

                        x=False
                                  
                        
                        __M_locals_builtin_stored = __M_locals_builtin()
                        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                        # SOURCE LINE 32
                        __M_writer(u'\n')
                        # SOURCE LINE 33
                    else:
                        # SOURCE LINE 34
                        __M_writer(u'\t\t\t\t')
                        __M_writer(escape(roweven(file.filename,url(controller='view',action='file',id=file.id))))
                        __M_writer(u'\n\t\t\t')
                        # SOURCE LINE 35

                        x=True            
                                          
                        
                        __M_locals_builtin_stored = __M_locals_builtin()
                        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                        # SOURCE LINE 37
                        __M_writer(u'\n')
                        pass
                    pass
                # SOURCE LINE 40
                __M_writer(u'  </ul>\n')
                pass
            # SOURCE LINE 42
            __M_writer(u'</form>\n')
            # SOURCE LINE 43
            if c.grade is not None:
                # SOURCE LINE 44
                __M_writer(u'<div id="grade">\n<strong>Grade: </strong>')
                # SOURCE LINE 45
                __M_writer(escape(c.grade))
                __M_writer(u'\n</div>\n<div id="comments">\n<strong>Instructor comments:</strong> ')
                # SOURCE LINE 48
                __M_writer(escape(c.comments))
                __M_writer(u'\n</div>\n')
                pass
            pass
        # SOURCE LINE 52
        __M_writer(u'</div>\n</body></html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


