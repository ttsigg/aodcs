# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349917699.975141
_enable_loop = True
_template_filename = '/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/login.mako'
_template_uri = '/login.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n<html><head>\n<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"><title>AODCS</title>\n\n\n\n')
        # SOURCE LINE 7
        __M_writer(escape(h.tags.stylesheet_link('/styles/style.css')))
        __M_writer(u'\n\n</head>\n<body>\n\t\t<div id="header">\n\t\t\t<h1> AODCS</h1><br><br>\n\t\t</div>\n\t\t<br><br>\n\t\t\n\t\t')
        # SOURCE LINE 16
        messages = h.flash.pop_messages() 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['messages'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        # SOURCE LINE 17
        if messages:
            # SOURCE LINE 18
            __M_writer(u'\t\t<ul id="flash-messages">\n')
            # SOURCE LINE 19
            for message in messages:
                # SOURCE LINE 20
                __M_writer(u'\t\t\t<li>')
                __M_writer(escape(message))
                __M_writer(u'</li>\n')
            # SOURCE LINE 22
            __M_writer(u'\t\t</ul>\n')
        # SOURCE LINE 24
        __M_writer(u'\t\n\t\t<div style="text-align: center; vertical-align: middle; postion: relative; top: -200px;">\n\t\t')
        # SOURCE LINE 26
        __M_writer(escape(h.tags.form("/login/submit", method="post")))
        __M_writer(u'\n\t\tUsername: ')
        # SOURCE LINE 27
        __M_writer(escape(h.tags.text("username")))
        __M_writer(u'<br>\n\t\tPassword: ')
        # SOURCE LINE 28
        __M_writer(escape(h.tags.password("password")))
        __M_writer(u'<br>\n\t\t')
        # SOURCE LINE 29
        __M_writer(escape(h.tags.submit("login","Login")))
        __M_writer(u'\n\t\t')
        # SOURCE LINE 30
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n\n<!--\t\tUsernames and passwords are:<br>\n\t\t<strong>student</strong> default<br>\n\t\t<strong>faculty</strong> default<br>\n\t\t<strong>admin</strong> default<br>\n\t\t-->\n\n\t</div>\n</body>\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


