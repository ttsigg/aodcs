# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349917667.17972
_enable_loop = True
_template_filename = u'/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/header.mako'
_template_uri = u'/header.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<h1>AODCS</h1><h3>')
        __M_writer(escape(c.role))
        __M_writer(u'</h3>\n<div id="loginbox">\n')
        # SOURCE LINE 3
        runtime._include_file(context, u'loginbox.mako', _template_uri)
        __M_writer(u'\n</div>\n<br>\n')
        # SOURCE LINE 6
        messages = h.flash.pop_messages() 
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['messages'] if __M_key in __M_locals_builtin_stored]))
        __M_writer(u'\n')
        # SOURCE LINE 7
        if messages:
            # SOURCE LINE 8
            __M_writer(u'<ul id="flash-messages">\n')
            # SOURCE LINE 9
            for message in messages:
                # SOURCE LINE 10
                __M_writer(u'   <li>')
                __M_writer(escape(message))
                __M_writer(u'</li>\n')
            # SOURCE LINE 12
            __M_writer(u'</ul>\n')
        # SOURCE LINE 14
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


