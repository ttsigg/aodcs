# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1291767216.043507
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/editinfo.mako'
_template_uri='/editinfo.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        url = context.get('url', UNDEFINED)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        str = context.get('str', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n<div id="main">\n')
        # SOURCE LINE 3
        __M_writer(escape(h.tags.form(url(controller='prof',action='save',id=c.fid))))
        __M_writer(u'\n  <h2>Edit User</h2>\n  <br>\n  <table>\n    <tbody>\n      <tr>\n\t\t\t\t<td>Username:</td>\n\t\t\t\t<td>')
        # SOURCE LINE 10
        __M_writer(escape(h.tags.text('username',value=c.fusername)))
        __M_writer(u'</td>\n        <td>Name:</td>\n        <td>')
        # SOURCE LINE 12
        __M_writer(escape(h.tags.text('name',value=c.fname)))
        __M_writer(u'</td>\n      </tr>\n      <tr>\n        <td>Bio:</td>\n        <td colspan=3>')
        # SOURCE LINE 16
        __M_writer(escape(h.tags.textarea('bio',cols=50, rows=8,content=c.fbio)))
        __M_writer(u'</textarea></td>\n      </tr>\n      <tr>\n      <tr>\n        <td>Password:</td>\n        <td>')
        # SOURCE LINE 21
        __M_writer(escape(h.tags.password('pass1')))
        __M_writer(u'</td>    \n        <td>Confirm Password:</td>\n        <td>')
        # SOURCE LINE 23
        __M_writer(escape(h.tags.password('pass2')))
        __M_writer(u'</td>\n      </tr>\n\t\t\t<tr>\n\t\t\t<td>Portfolio URL:</td>\n\t\t\t<td>')
        # SOURCE LINE 27
        __M_writer(escape(h.tags.link_to(h.url('/p/'+str(c.fid)),h.url('/p/'+str(c.fid)))))
        __M_writer(u'</td>\n\t\t\t</tr>\n\t\t\t<tr>\n\t\t\t<td>Role:</td>\n')
        # SOURCE LINE 31
        if c.role == 'Administrator':
            # SOURCE LINE 32
            __M_writer(u'\t\t\t<td>')
            __M_writer(escape(h.tags.select('role',c.frole,c.froles)))
            __M_writer(u'</td>\n')
            # SOURCE LINE 33
        else:
            # SOURCE LINE 34
            __M_writer(u'\t\t\t<td>')
            __M_writer(escape(c.frole[1]))
            __M_writer(u'</td>\n')
            pass
        # SOURCE LINE 36
        __M_writer(u'\t\t\t\n    </tbody>\n  </table>\n  <input value="Save" name="Save" type="submit">\n\t')
        # SOURCE LINE 40
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n</div>\n\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


