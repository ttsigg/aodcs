# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1291771981.752722
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/portfolio.mako'
_template_uri='/portfolio.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    # SOURCE LINE 1
    ns = runtime.Namespace('__anon_0x2745810', context._clean_inheritance_tokens(), templateuri=u'functions.mako', callables=None, calling_uri=_template_uri, module=None)
    context.namespaces[(__name__, '__anon_0x2745810')] = ns

def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x2745810')._populate(_import_ns, [u'rowodd', u'roweven', u'sidebar'])
        c = _import_ns.get('c', context.get('c', UNDEFINED))
        url = _import_ns.get('url', context.get('url', UNDEFINED))
        h = _import_ns.get('h', context.get('h', UNDEFINED))
        str = _import_ns.get('str', context.get('str', UNDEFINED))
        roweven = _import_ns.get('roweven', context.get('roweven', UNDEFINED))
        sidebar = _import_ns.get('sidebar', context.get('sidebar', UNDEFINED))
        rowodd = _import_ns.get('rowodd', context.get('rowodd', UNDEFINED))
        __M_writer = context.writer()
        __M_writer(u'\n<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">\n\n<html><head>\n\t\t<meta content="text/html; charset=ISO-8859-1" http-equiv="content-type">\n\t\t<link rel="stylesheet" href="/styles/style.css" type="text/css">\n\t\t<title>Portfolio: ')
        # SOURCE LINE 7
        __M_writer(escape(c.name))
        __M_writer(u'</title>\n</head>\n<div id="header">\n<h1>Portfolio for ')
        # SOURCE LINE 10
        __M_writer(escape(c.name))
        __M_writer(u'</h1>\n<br>\n<br></div>\n')
        # SOURCE LINE 13
        __M_writer(escape(sidebar(['/p/'+str(c.id),'Portfolio'],c.listitems,['/portfolios/resume/'+str(c.id),'Resume'])))
        __M_writer(u'\n<br>\n<div id="main">\n\t<h3>')
        # SOURCE LINE 16
        __M_writer(escape(c.project))
        __M_writer(u'</h3>\n')
        # SOURCE LINE 17
        if c.project != 'No Project':
            # SOURCE LINE 18
            __M_writer(escape(h.tags.form('files')))
            __M_writer(u'\n')
            # SOURCE LINE 19
            if c.files:
                # SOURCE LINE 20
                __M_writer(u'<ul>\n')
                # SOURCE LINE 21

                x=False
                
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                # SOURCE LINE 23
                __M_writer(u'\n')
                # SOURCE LINE 24
                for file in c.files:
                    # SOURCE LINE 25
                    if x: 
                        # SOURCE LINE 26
                        __M_writer(u'\t\t\t')
                        __M_writer(escape(rowodd(file.filename,url(controller='view',action='file',id=c.id,project=c.project))))
                        __M_writer(u'\n\t\t\t')
                        # SOURCE LINE 27

                        x=False
                                  
                        
                        __M_locals_builtin_stored = __M_locals_builtin()
                        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                        # SOURCE LINE 29
                        __M_writer(u'\n')
                        # SOURCE LINE 30
                    else:
                        # SOURCE LINE 31
                        __M_writer(u'\t\t\t')
                        __M_writer(escape(roweven(file.filename,url(controller='view',action='file',id=c.id, project=c.project))))
                        __M_writer(u'\n\t\t\t')
                        # SOURCE LINE 32

                        x=True            
                                          
                        
                        __M_locals_builtin_stored = __M_locals_builtin()
                        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['x'] if __M_key in __M_locals_builtin_stored]))
                        # SOURCE LINE 34
                        __M_writer(u'\n')
                        pass
                    pass
                # SOURCE LINE 37
                __M_writer(u'  </ul>\n')
                pass
            # SOURCE LINE 39
            __M_writer(u'</form>\n')
            pass
        # SOURCE LINE 41
        __M_writer(u'</div>\n\n</html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


