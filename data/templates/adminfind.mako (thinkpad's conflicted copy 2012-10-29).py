# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 8
_modified_time = 1349918666.625578
_enable_loop = True
_template_filename = '/home/tim/school/fa10/sse/thorshammer2/thorshammer/thorshammer/templates/adminfind.mako'
_template_uri = 'adminfind.mako'
_source_encoding = 'utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    # SOURCE LINE 2
    ns = runtime.TemplateNamespace('__anon_0x7f41780bbfd0', context._clean_inheritance_tokens(), templateuri=u'functions.mako', callables=None,  calling_uri=_template_uri)
    context.namespaces[(__name__, '__anon_0x7f41780bbfd0')] = ns

def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'/base.mako', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _import_ns = {}
        _mako_get_namespace(context, '__anon_0x7f41780bbfd0')._populate(_import_ns, [u'rowodd', u'roweven'])
        h = _import_ns.get('h', context.get('h', UNDEFINED))
        c = _import_ns.get('c', context.get('c', UNDEFINED))
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n')
        # SOURCE LINE 2
        __M_writer(u'\n\n<h3>Search Results</h3><br>\n')
        # SOURCE LINE 5
        if c.users:
            # SOURCE LINE 6
            __M_writer(u"<ul id='results'>\n")
            # SOURCE LINE 7
            for user in c.users:
                # SOURCE LINE 8
                __M_writer(u'\t<li>')
                __M_writer(escape(user.name))
                __M_writer(u'\t\n\t')
                # SOURCE LINE 9
                __M_writer(escape(h.tags.link_to('edit',url=h.url(controller='prof',action='edit',id=user.username))))
                __M_writer(u'\t\n\t')
                # SOURCE LINE 10
                __M_writer(escape(h.tags.link_to('delete',url=h.url(controller='admin',action='delete',id=user.username))))
                __M_writer(u'\n\t</li>\n')
            # SOURCE LINE 13
            __M_writer(u'\t</ul>\n')
        # SOURCE LINE 15
        __M_writer(u'\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


