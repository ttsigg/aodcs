# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1290533643.931292
_template_filename='/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/newproject.mako'
_template_uri='newproject.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, u'base.mako', _template_uri)
def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'\n\n<div id="main">\n\t')
        # SOURCE LINE 4
        __M_writer(escape(h.tags.form("/project/create", method="post")))
        __M_writer(u'\n  <h2>New Project<br>\n  </h2>\n  <br>\n  <table>\n    <tbody>\n      <tr>\n        <td>Name:</td>\n\t\t\t\t<td colspan="3">')
        # SOURCE LINE 12
        __M_writer(escape(h.tags.text("name")))
        __M_writer(u'</td>\n      </tr>\n      <tr>\n        <td>Description<br>\n        </td>\n\t\t\t\t<td colspan="3">')
        # SOURCE LINE 17
        __M_writer(escape(h.tags.textarea("desc", cols=51, rows=8)))
        __M_writer(u'</textarea></td>\n      </tr>\n      <tr>\n        <td>Class:</td>\n        <td>\n        ')
        # SOURCE LINE 22
        __M_writer(escape(h.tags.select('course','',c.courses)))
        __M_writer(u'\n        </td>\n        <td> Group Members: </td>\n        <td>\n        ')
        # SOURCE LINE 26
        __M_writer(escape(h.tags.select('users',c.userid,c.users,multiple=True)))
        __M_writer(u'\n        </td>\n      </tr>\n    </tbody>\n  </table><br>\n')
        # SOURCE LINE 31
        __M_writer(escape(h.tags.submit("Save","Save")))
        __M_writer(u'\n')
        # SOURCE LINE 32
        __M_writer(escape(h.tags.end_form()))
        __M_writer(u'\n</div>\n\n</body></html>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


