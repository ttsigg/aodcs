# -*- encoding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 5
_modified_time = 1291594155.128047
_template_filename=u'/home/tim/school/thorshammer2/thorshammer/thorshammer/templates/loginbox.mako'
_template_uri=u'/loginbox.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='utf-8'
from webhelpers.html import escape
_exports = []


def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        c = context.get('c', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1
        __M_writer(u'<div id="loginbox">Logged in as ')
        __M_writer(escape(c.name))
        __M_writer(u' <br>\n[<a href="/prof/edit/')
        # SOURCE LINE 2
        __M_writer(escape(c.userid))
        __M_writer(u'">settings</a>] \n[<a href="http://www.google.com/">help</a>] \n[<a href="/login/logout">logout</a>]</div>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


